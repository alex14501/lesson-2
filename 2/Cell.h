#pragma once
#include <iostream>
#include "Nucleus.h"
#include "Ribosome.h"
#include "Mitochondrion.h"
#include "Gene.h"
class Cell
{
public:
	//intiating function
	void init(const std::string dna_sequence, const Gene _glucose_receptor_gene);
	//Getters
	/*energey producing function(sort of main function)*/
	bool get_ATP();
	Nucleus getNucleus() const;
	Ribosome getRibosome() const;
	Gene getGlucoseReceptorGene() const;
	Mitochondrion getMitochondrion() const;
	unsigned int getAtpUnits() const;
	//Setters
	void setNucleus(Nucleus nucleus);
	void setRibosome(Ribosome ribosome);
	void setMitochondrion(Mitochondrion mitochondrion);
	void setGene(Gene gene);
	void setAtpUnits(unsigned int atpUnits);
private:
	Nucleus _nucleus;
	Ribosome _ribosome;
	Mitochondrion _mitochondrion;
	Gene _glucose_receptor_gene;
	unsigned int _atp_units;
};