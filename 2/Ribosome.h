#pragma once
#include <iostream>
#include "Protein.h"
class Ribosome
{
public:
	//this function creates a protein according to an RNA
	Protein* create_protein(std::string &RNA_transcript) const;
};