////////////////////////////////////////
///cell function program////////////////
///made by alex/////////////////////////
////////////////////////////////////////
#include <iostream>
#include "Cell.h"
#define FULL_ATP_UNITS 100
#define HALF_ATP_UNITS 50
using std::cout;
using std::endl;
using std::string;
void Cell::init(const std::string dna_sequence, const Gene glucose_receptor_gene)
{
	this->_nucleus.init(dna_sequence);
	this->_mitochondrion.init();
	this->_glucose_receptor_gene = glucose_receptor_gene;
	this->_atp_units = 0;
}
//Getters
bool Cell::get_ATP()
{
	string RNA = "";
	Protein* protein;
	RNA = this->_nucleus.get_RNA_transcript(this->_glucose_receptor_gene);
	protein=this->_ribosome.create_protein(RNA);
	if (protein != nullptr)
	{
		this->_mitochondrion.insert_glucose_receptor(*protein);
	}
	else
	{
		std::cerr << "Ribosome was unable to create protein existing...\n";
		_exit(1);
	}
	this->_mitochondrion.setGlucoseLevel(HALF_ATP_UNITS);
	if (this->_mitochondrion.produceATP())
	{
		this->_atp_units = FULL_ATP_UNITS;
		return true;
	}
	else
	{
		return false;
	}
}
Nucleus Cell::getNucleus() const
{
	return this->_nucleus;
}
Ribosome Cell::getRibosome() const
{
	return this->_ribosome;
}
Gene Cell::getGlucoseReceptorGene() const
{
	return this->_glucose_receptor_gene;
}
Mitochondrion Cell::getMitochondrion() const
{
	return this->_mitochondrion;
}

unsigned int Cell::getAtpUnits() const
{
	return this->_atp_units;
}
//Setters
void Cell::setNucleus(Nucleus nucleus)
{
	this->_nucleus = nucleus;
}
void Cell::setRibosome(Ribosome ribosome)
{
	this->_ribosome = ribosome;
}
void Cell::setMitochondrion(Mitochondrion mitochondrion)
{
	this->_mitochondrion = mitochondrion;
}
void Cell::setGene(Gene gene)
{
	this->_glucose_receptor_gene = gene;
}
void Cell::setAtpUnits(unsigned int atpUnits)
{
	this->_atp_units = atpUnits;
}