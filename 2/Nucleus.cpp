///////////////////////////////
//nucleus class////////////////
//made by alex/////////////////
///////////////////////////////
#include <iostream>
#include "Nucleus.h"
#define END_OF_CODON 2
using std::cout;
using std::endl;
using std::string;
//this function intiates a DNA and creates a Complimentary dna
void Nucleus::init(const std::string dna_sequence)
{
	this->_DNA_strand = dna_sequence;
	string complimentaryDna = dna_sequence;
	for (unsigned int i = 0; i < complimentaryDna.length(); i++)//making complimentary DNA
	{
		if (complimentaryDna[i] == 'G')
		{
			complimentaryDna[i] = 'C';
		}
		else
		{
			if (complimentaryDna[i] == 'C')
			{
				complimentaryDna[i] = 'G';
			}
			else
			{
				if (complimentaryDna[i] == 'T')
				{
					complimentaryDna[i] = 'A';
				}
				else
				{
					if (complimentaryDna[i] == 'A')
					{
						complimentaryDna[i] = 'T';
					}
					else
					{
						std::cerr << "illeagal charcters\n";
						_exit(1);
					}
				}
			}
		}
	}
	this->_complementary_DNA_strand = complimentaryDna;
}
//Getters
//this function return and creates an RNA transcript
std::string Nucleus::get_RNA_transcript(const Gene& gene) const
{
	string RNA = "";
	string DNA = "";
	if (!gene.is_on_complementary_dna_strand())
	{
		DNA = this->_DNA_strand;
	}
	else
	{
		DNA = this->_complementary_DNA_strand;
	}
	for (unsigned int i = gene.getStart(); i <= gene.getEnd(); i++)
	{
		if (DNA[i] == 'T')
		{
			
			RNA += 'U';
		}
		else
		{
			RNA += DNA[i];
		}
	}
	return RNA;
}
//this function revrses the DNA strand
std::string Nucleus::get_reversed_DNA_strand() const
{
	string reversedDna = "";
	for (int i = this->_DNA_strand.length() - 1; i >= 0; i--)
	{
		reversedDna += this->_DNA_strand[i];
	}
	return reversedDna;
}
// this function counts the number of times that a codon appears
unsigned int Nucleus::get_num_of_codon_appearances(const std::string& codon) const
{
	int numOfCodons = 0;
	string DNA = this->_DNA_strand;
	while (DNA.find(codon)!=string::npos)
	{
		numOfCodons++;
		if (DNA.find(codon) + END_OF_CODON < DNA.length() - 1)
		{
			DNA.substr(DNA.find(codon) + END_OF_CODON);//making a new string to search codons in
		}
	}
	return numOfCodons;
}