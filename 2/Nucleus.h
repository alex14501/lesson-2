#pragma once
#include <iostream>
#include "Gene.h"
class Nucleus
{
public:
	//intiating function
	void init(const std::string dna_sequence);
	//Getters
	//this function creant an RNA
	std::string get_RNA_transcript(const Gene& gene) const;
	//this function reverses an DNA
	std::string get_reversed_DNA_strand() const;
	//this function counts the times of one codon appears
	unsigned int get_num_of_codon_appearances(const std::string& codon) const;
private:
	std::string  _DNA_strand;
	std::string _complementary_DNA_strand;
};