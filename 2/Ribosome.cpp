///////////////////////////////////
////ribosome class/////////////////
////made by alex///////////////////
///////////////////////////////////
#include <iostream>
#include "Ribosome.h"
#include "Protein.h"
#include "AminoAcid.h"
#define CODON_LENGTH 3
using std::cout;
using std::endl;
using std::string;
/*
this function creates a protein according to the database
----------------------------
input
----------------------------
RNA transcript
----------------------------
output
----------------------------
complete protein
*/
Protein* Ribosome::create_protein(std::string &RNA_transcript) const
{
	Protein* protein=new Protein;
	AminoAcid aminoAcid;
	string chain = "";
	string RNA = RNA_transcript;
	bool flag = true;
	(*protein).init();
	while (RNA.length() >= CODON_LENGTH &&flag)
	{
		chain = RNA.substr(0, CODON_LENGTH);
		if (get_amino_acid(chain) != UNKNOWN)
		{
			aminoAcid = get_amino_acid(chain);
			(*protein).add(aminoAcid);
		}
		else
		{
			flag = false;
		}
		if (RNA.length() >= CODON_LENGTH)
		{
			RNA=RNA.substr(CODON_LENGTH);//might be a point of failure
		}
	}
	if (!flag)
	{
		(*protein).clear();
		return nullptr;
	}
	else
	{
		return protein;
	}
}