#pragma once
#include <iostream>
#include "Protein.h"
class Mitochondrion
{
public:
	void init();
	//this makes a glucose receptor for inputed protein
	void insert_glucose_receptor(const Protein& protein);
	//this function produces ATP
	bool produceATP() const;
	//Getters
	unsigned int getGlocuse_level() const;
	bool check_glucose_receptor() const;
	//Setters
	void setGlucoseLevel(const unsigned int glucoseLevel);
	void setGlucoseReceptor(const bool glucoseReceptor);
private:
	unsigned int _glocuse_level;
	bool _has_glucose_receptor;
};