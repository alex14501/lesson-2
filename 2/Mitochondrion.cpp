#include <iostream>
#include "Mitochondrion.h"
#define HALF_GLUCOSE_LEVEL 50
using std::cout;
using std::endl;
using std::string;
//this function intiates a mitochondrion
void Mitochondrion::init()
{
	this->_glocuse_level = 0;
	this->_has_glucose_receptor = false;
}
/*
this function cheks for correct glucose receptor
---------------------------------
input-
---------------------------------
protein(glucose receptor)
---------------------------------
output
---------------------------------
none
*/
void Mitochondrion::insert_glucose_receptor(const Protein& protein)
{
	AminoAcidNode* aminoAcid;
	aminoAcid = protein.get_first();
	if ((*aminoAcid).get_data() == AminoAcid::ALANINE)
	{
		aminoAcid = aminoAcid->get_next();
		if ((*aminoAcid).get_data() == AminoAcid::LEUCINE)
		{
			aminoAcid = aminoAcid->get_next();
			if ((*aminoAcid).get_data() == AminoAcid::GLYCINE)
			{
				aminoAcid = aminoAcid->get_next();
				if ((*aminoAcid).get_data() == AminoAcid::HISTIDINE)
				{
					aminoAcid = aminoAcid->get_next();
					if ((*aminoAcid).get_data() == AminoAcid::LEUCINE)
					{
						aminoAcid = aminoAcid->get_next();
						if ((*aminoAcid).get_data() == AminoAcid::PHENYLALANINE)
						{
							aminoAcid = aminoAcid->get_next();
							if ((*aminoAcid).get_data() == AminoAcid::AMINO_CHAIN_END)
							{
								this->_has_glucose_receptor = true;
							}
						}
					}
				}
			}
		}
	}

}
//this function sets or "produces ATP_level"
bool Mitochondrion::produceATP() const
{
	if (this->_glocuse_level >= HALF_GLUCOSE_LEVEL && this->_has_glucose_receptor)
	{
		return true;
	}
	else
	{
		return false;
	}
}
//Getters
unsigned int Mitochondrion::getGlocuse_level() const
{
	return this->_glocuse_level;
}
bool Mitochondrion::check_glucose_receptor() const
{
	return this->_has_glucose_receptor;
}
//Setters
void Mitochondrion::setGlucoseLevel(const unsigned int glucoseLevel)
{
	this->_glocuse_level = glucoseLevel;
}

void Mitochondrion::setGlucoseReceptor(const bool glucoseReceptor)
{
	this->_has_glucose_receptor = glucoseReceptor;
}